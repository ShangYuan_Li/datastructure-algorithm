# datastructure-algorithm

#### 介绍
**数据结构和算法学习记录**

---

**基础**
1. 位运算-BitArithmetic
2. 阶乘算法-SumOfFactorial
3. 选择排序-SelectSort
4. 冒泡排序-BubbleSort
5. 插入排序-InsertionSort
6. 数组分段求和-PreSum
7. 随机数计算(x^2)-RandToRand
8. 1-5等概率，获取1-7等概率整数-RandToRand
9. 不等0，1随机，获取相等0，1随机-RandToRand
10. 对数器(创建例子调试)-Comp
11. 二分法，查找目标值(有序数组)-BSExist
12. 二分法查询目标值最左位置(有序数组)-BSNearLeft
13. 二分法查询局部最小(大)值-BSAwesome
14. 哈希表和有序表-HashMapTreeMap
15. 单链表的反转-ReverseLinkedList
16. 双链表的反转-ReverseDoubleList
17. 单链表实现队列-LinkedListToQueue
18. 单链表实现栈-LinkedListToStack
19. 双链表实现双端队列-DoubleLinkedListToDeque
20. k个节点的组内逆序节点(leetcode25)-ReverseNodesInKGroup
21. 两个链表相加(leetcode2)-AddTwoNumbers
22. 有序链表合并(leetcode21)-MergeTwoSortedLinkedList
23. 位图-BitMap
24. 位运算实现加减乘除(leetcode29)-BitAddMinusMultiDiv
25. 比较器-ShowComparator
26. 合并k个升序链表(leetcode23)-MergeKSortedLists
27. 二叉树遍历-TraversalBinaryTree
28. 相同二叉树(leetcode100)-SameTree
29. 对称二叉树(leetcode101)-SymmetricTree
30. 二叉树的最大深度(leetcode104)-MaximumDepthOfBinaryTree
31. 从前序与中序遍历序列构造二叉树(leetcode105)-ConstructBinaryTreeFromPreorderAndInorderTraversal
32. 二叉树的层序倒叙遍历(leetcode107)-BinaryTreeLevelOrderTraversalII
33. 平衡二叉树(leetcode110)-BalancedBinaryTree
34. 搜索二叉树(leetcode98)-IsBinarySearchTree
35. 路径总和(leetcode112)-PathSum
36. 收集符合条件路径总和(leetcode113)-PathSumII
37. 归并排序-MergeSort
38. 快速排序-PartitionAndQuickSort

---

**体系**
1. 选择排序-SelectionSort
2. 冒泡排序-BubbleSort
3. 插入排序-InsertionSort
4. 二分法，查找目标值(有序数组)-BSExist
5. 二分法查询目标值最左位置(有序数组)-BSNearLeft
6. 二分法查询目标值最右位置(有序数组)-BSNearRight
7. 二分法查询局部最小值(相邻不等)-BSAwesome
8. 位运算-BitArithmetic
9. 不用额外变量交换两个数-Swap
10. 打印出现奇数次的数-EvenTimesOddTimes
11. KM出现次数问题-KM

---
