package basics.datastructure.map;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * 哈希表和有序表
 * 
 * @author lisy
 */
public class HashMapTreeMap {
	
	public static class Node {
		public int value;

		public Node(int v) {
			value = v;
		}
	}

	// (K V)表
	public static void main(String[] args) {
		// 哈希表
		HashMap<String, String> map = new HashMap<>();
		map.put("li", "lisy");
		System.out.println(map.containsKey("li"));
		System.out.println(map.containsKey("l"));
		// 时间复杂度为1
		System.out.println(map.get("li"));
		map.put("li", "sy");
		System.out.println(map.get("li"));

		// 按值传递
		String test1 = "li";
		String test2 = "li";
		System.out.println(test1 == test2);
		System.out.println(map.containsKey(test1));
		System.out.println(map.containsKey(test2));

		map.remove("li");
		System.out.println(map.containsKey("li"));
		System.out.println(map.get("li"));
		System.out.println("===================");
		
		// 非基础结构按引用传递
		Node node1 = new Node(1);
		Node node2 = new Node(1);
		HashMap<Node, String> map3 = new HashMap<>();
		map3.put(node1, "我进来了！");
		System.out.println(map3.containsKey(node1));
		System.out.println(map3.containsKey(node2));
		System.out.println("===================");

		// 有序表
		TreeMap<Integer, String> treeMap1 = new TreeMap<>();
		treeMap1.put(3, "我是3");
		treeMap1.put(0, "我是3");
		treeMap1.put(7, "我是3");
		treeMap1.put(2, "我是3");
		treeMap1.put(5, "我是3");
		treeMap1.put(9, "我是3");

		System.out.println(treeMap1.containsKey(7));
		System.out.println(treeMap1.containsKey(6));
		System.out.println(treeMap1.get(3));

		treeMap1.put(3, "他是3");
		System.out.println(treeMap1.get(3));

		treeMap1.remove(3);
		System.out.println(treeMap1.get(3));

		// 最小key
		System.out.println(treeMap1.firstKey());
		// 最大key
		System.out.println(treeMap1.lastKey());
		// <=5 离5最近的key告诉我
		System.out.println(treeMap1.floorKey(5));
		// <=6 离6最近的key告诉我
		System.out.println(treeMap1.floorKey(6));
		// >=5 离5最近的key告诉我
		System.out.println(treeMap1.ceilingKey(5));
		// >=6 离6最近的key告诉我
		System.out.println(treeMap1.ceilingKey(6));

		// 上述方法要求 key 可以比较， Node 需要传入比较器

	}
}
