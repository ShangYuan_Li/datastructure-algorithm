package basics.datastructure.list;

import java.util.ArrayList;
import java.util.List;

/**
 * 单链表反转
 * 
 * @author lisy
 */
public class ReverseLinkedList {

	/**
	 * 单链表
	 */
	public static class Node {
		public int value;
		public Node next;

		public Node(int data) {
			value = data;
		}
	}
	
	/**
	 * 自动生成单链表
	 */
	public static Node generateRandomLinkedList(int len, int value) {
		int size = (int) (Math.random() * (len + 1));
		if (size == 0) {
			return null;
		}
		size--;
		Node head = new Node((int) (Math.random() * (value + 1)));
		Node pre = head;
		while (size != 0) {
			Node cur = new Node((int) (Math.random() * (value + 1)));
			pre.next = cur;
			pre = cur;
			size--;
		}
		return head;
	}
	
	public static List<Integer> getLinkedListOriginOrder(Node head) {
		List<Integer> ans = new ArrayList<Integer>();
		while (head != null) {
			ans.add(head.value);
			head = head.next;
		}
		return ans;
	}
	
	/**
	 * 单链表反转比较器
	 */
	public static boolean checkLinkedListReverse(List<Integer> origin, Node head) {
		for (int i = origin.size() - 1; i >= 0; i--) {
			if (!origin.get(i).equals(head.value)) {
				return false;
			}
			head = head.next;
		}
		return true;
	}
	
	/**
	 * 单链表反转
	 */
	public static Node reverseLinkedList(Node head) {
		Node pre = null;
		Node next = null;
		while (head != null) {
			next = head.next;
			head.next = pre;
			pre = head;
			head = next;
		}
		return pre;
	}
	
	public static void main(String[] args) {
		int len = 50;
		int value = 100;
		int testTime = 100000;
		System.out.println("test begin!");
		for (int i = 0; i < testTime; i++) {
			
			// 单链表反转
			Node node1 = generateRandomLinkedList(len, value);
			List<Integer> list1 = getLinkedListOriginOrder(node1);
			node1 = reverseLinkedList(node1);
			if (!checkLinkedListReverse(list1, node1)) {
				System.out.println("Oops1!");
			}

		}
		System.out.println("test finish!");
	}
}
