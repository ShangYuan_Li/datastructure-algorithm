package basics.datastructure.tree;

/**
 * 判断两个二叉树相同
 * 
 * @author lisy
 */
public class SameTree {
	
	// 测试链接：https://leetcode.com/problems/same-tree
	public static class TreeNode {
		public int val;
		public TreeNode left;
		public TreeNode right;
	}

	public static boolean isSameTree(TreeNode p, TreeNode q) {
		if (p == null ^ q == null) {
			return false;
		}
		if (p == null && q == null) {
			return true;
		}
		// 都不为空
		return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
	}
}
