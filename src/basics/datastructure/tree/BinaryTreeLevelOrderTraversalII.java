package basics.datastructure.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 二叉树的层序遍历-倒叙
 * 
 * @author lisy
 */
public class BinaryTreeLevelOrderTraversalII {

	// 测试链接：https://leetcode.com/problems/binary-tree-level-order-traversal-ii
	public static class TreeNode {
		public int val;
		public TreeNode left;
		public TreeNode right;

		TreeNode(int val) {
			this.val = val;
		}
	}

	public List<List<Integer>> levelOrderBottom(TreeNode root) {
		List<List<Integer>> ans = new LinkedList<>();
		if (root == null) {
			return ans;
		}
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int size = queue.size();
			List<Integer> curAns = new LinkedList<>();
			for (int i = 0; i < size; i++) {
				TreeNode curNode = queue.poll();
				curAns.add(curNode.val);
				if (curNode.left != null) {
					queue.add(curNode.left);
				}
				if (curNode.right != null) {
					queue.add(curNode.right);
				}
			}
			ans.add(0, curAns);
		}
		return ans;
	}
	
	public static void main(String[] args) {
		// 测试ArrayList和LinkedList效率
		int testTime = 1000000;
		long start;
		long end;
		System.out.println("hello");
		ArrayList<Integer> arr = new ArrayList<>();
		start = System.currentTimeMillis();
		for (int i = 0; i < testTime; i++) {
			arr.add(0, i);
		}
		end = System.currentTimeMillis();
		System.out.println(end - start);
		
		LinkedList<Integer> arr2 = new LinkedList<>();
		start = System.currentTimeMillis();
		for (int i = 0; i < testTime; i++) {
			arr2.add(0, i);
		}
		end = System.currentTimeMillis();
		System.out.println(end - start);
	}
}
