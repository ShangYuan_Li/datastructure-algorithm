package basics.algorithm.bit;

/**
 * 位运算
 * 
 * @author lisy
 */
public class BitArithmetic {
	
	public static void print(int num) {
		for (int i = 31; i >= 0; i--) {
			System.out.print((num & (1 << i)) == 0 ? "0" : "1");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		
		System.out.println("------左移-----");
		
		int num = -4;
		print(num);
		// 左移1位相当于num*2
		print(num<<1);
		
		System.out.println("------正数最大值-----");
		
		// 第一位表示正(0)负(1)，后31位表示值
		int maxInt = Integer.MAX_VALUE;
		System.out.println(maxInt);
		print(maxInt);
		
		System.out.println("------取反-----");
		
		// 取反
		int b = 12341234;
		int c = ~b;
		print(b);
		print(c);
		
		System.out.println("------负数最大值-----");
		
		// 0归在非负区，最大:2147483647，最小:-2147483648
		// 负数的值：除符号位取反+1,目的:计算走一套逻辑提升性能
		print(-1);
		int minInt = Integer.MIN_VALUE;
		System.out.println(minInt);
		print(minInt);
		
		System.out.println("------右移-----");
		
		// 右移  >>:用符号位补; >>>:用0补
		print(minInt>>1);
		print(minInt>>>1);
		
		System.out.println("------或、与、异或运算-----");
		
		// 或运算(有1是1)、与运算(都是1是1)、异或运算(相同为0不同为1)
		int e = 1234;
		int f = 15678;
		print(e);
		print(f);
		System.out.println("==========");
		print(e | f);
		print(e & f);
		print(e ^ f);
		
		System.out.println("------相反数-----");
		
		// 相反数，取反+1，正负数可以走一套逻辑
		// 系统最小和0的相反数是自己
		int g = 5;
		int h = (~g + 1);
		System.out.println(g);
		System.out.println(h);
		
	}
}
