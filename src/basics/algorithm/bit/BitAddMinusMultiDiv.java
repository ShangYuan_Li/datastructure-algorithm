package basics.algorithm.bit;

/**
 * 位运算实现加减乘除
 * 
 * @author lisy
 */
public class BitAddMinusMultiDiv {
	
	// 测试链接：https://leetcode.com/problems/divide-two-integers
	// 加法 异或+进位
	public static int add(int a, int b) {
		int sum = a;
		while (b != 0) {
			sum = a ^ b;
			b = (a & b) << 1;
			a = sum;
		}
		return sum;
	}

	public static int negNum(int n) {
		// 相反数 取反+1
		return add(~n, 1);
	}
	
	// 减法
	public static int minus(int a, int b) {
		// a-b => a+b的相反数
		return add(a, negNum(b));
	}

	// 乘法
	public static int multi(int a, int b) {
		int res = 0;
		// 思路:按位乘后相加
		while (b != 0) {
			if ((b & 1) != 0) {
				res = add(res, a);
			}
			a <<= 1;
			b >>>= 1;
		}
		return res;
	}

	public static boolean isNeg(int n) {
		return n < 0;
	}
	
	// 除法 向下取整
	public static int div(int a, int b) {
		// 取正数操作
		int x = isNeg(a) ? negNum(a) : a;
		int y = isNeg(b) ? negNum(b) : b;
		int res = 0;
		// 思路:除数左移i到不大于被除数，记录一次结果
		for (int i = 30; i >= 0; i = minus(i, 1)) {
			if ((x >> i) >= y) {
				res |= (1 << i);
				x = minus(x, y << i);
			}
		}
		// 最后判断符号
		return isNeg(a) ^ isNeg(b) ? negNum(res) : res;
	}

	// 考虑系统最小值处理(无法取绝对值 0)
	public static int divide(int a, int b) {
		if (a == Integer.MIN_VALUE && b == Integer.MIN_VALUE) {
			return 1;
		} else if (b == Integer.MIN_VALUE) {
			return 0;
		} else if (a == Integer.MIN_VALUE) {
			if (b == negNum(1)) { // leetcode约定
				return Integer.MAX_VALUE; 
			} else {
				int c = div(add(a, 1), b);
				return add(c, div(minus(a, multi(c, b)), b));
			}
		} else {
			return div(a, b);
		}
	}
	
	public static void main(String[] args) {
		int a = 3;
		int b = -2;
		System.out.println(add(a, b));
		System.out.println(minus(a, b));
		System.out.println(multi(a, b));
		System.out.println(divide(a, b));
	}
}
