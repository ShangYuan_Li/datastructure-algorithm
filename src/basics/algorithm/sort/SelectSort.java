package basics.algorithm.sort;

/**
 * 选择排序
 * 
 * @author lisy
 */
public class SelectSort {
	
	public static void selectSort(int[] arr) {
		// 先考虑边界条件
		if (arr == null || arr.length < 2) {
			return;
		}
		int N = arr.length;
		// 范围，选择位数
		for (int i = 0; i < N; i++) {
			int minValueIndex = i;
			for (int j = i + 1; j < N; j++) {
				minValueIndex = arr[j] < arr[minValueIndex] ? j : minValueIndex;
			}
			swap(arr, i, minValueIndex);
		}
	}
	
	public static void swap(int[] arr, int i, int j) {
		int temp = arr[j];
		arr[j] = arr[i];
		arr[i] = temp;
	}
	
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		int[] arr = {7,1,3,5,8,9,4,6,2};
		printArray(arr);
		selectSort(arr);
		printArray(arr);
	}
}
