package basics.algorithm.comp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 比较器
 * 
 * @author lisy
 */
public class ShowComparator {


	public static class Student {
		public String name;
		public int id;
		public int age;

		public Student(String name, int id, int age) {
			this.name = name;
			this.id = id;
			this.age = age;
		}
	}
	
	public static class IdComparator implements Comparator<Student> {

		// 如果返回负数，认为第一个参数应该排在前面
		// 如果返回正数，认为第二个参数应该排在前面
		// 如果返回0，认为谁放前面无所谓
		@Override
		public int compare(Student o1, Student o2) {
			if (o1.id < o2.id) {
				return 1;
			} else if (o2.id < o1.id) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	public static class AgeComparator implements Comparator<Student> {

		// 如果返回负数，认为第一个参数应该排在前面
		// 如果返回正数，认为第二个参数应该排在前面
		// 如果返回0，认为谁放前面无所谓
		@Override
		public int compare(Student o1, Student o2) {
			if (o1.age < o2.age) {
				return 1;
			} else if (o2.age < o1.age) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
	public static void printStudents(Student[] students) {
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i].name + ", " + students[i].id + ", " + students[i].age);
		}
	}
	
	public static void main(String[] args) {
		int[] arr = { 8, 1, 4, 1, 6, 8, 4, 1, 5, 8, 2, 3, 0 };
		printArray(arr);
		Arrays.sort(arr);
		printArray(arr);
		
		Student s1 = new Student("student1", 5, 27);
		Student s2 = new Student("student2", 1, 17);
		Student s3 = new Student("student3", 4, 29);
		Student s4 = new Student("student4", 3, 9);
		Student s5 = new Student("student5", 2, 34);
		
		System.out.println("----- array -----");
		Student[] students = { s1, s2, s3, s4, s5 };
		printStudents(students);
		System.out.println("======= id sort");
		Arrays.sort(students, new IdComparator());
		printStudents(students);
		
		System.out.println("----- arrayList -----");
		ArrayList<Student> arrList = new ArrayList<>();
		arrList.add(s1);
		arrList.add(s2);
		arrList.add(s3);
		arrList.add(s4);
		arrList.add(s5);
		for (Student s : arrList) {
			System.out.println(s.name + ", " + s.id + ", " + s.age);
		}
		System.out.println("======= age sort");
		arrList.sort(new AgeComparator());
		for (Student s : arrList) {
			System.out.println(s.name + ", " + s.id + ", " + s.age);
		}
		
		System.out.println("----- PriorityQueue -----");
		/**
		 * 优先级队列(堆结构)，自动根据数值排序(小->大)
		 */
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>();
		heap.add(2);
		heap.add(5);
		heap.add(1);
		heap.add(4);
		heap.add(3);
		// 返回最小值，不抛出队列
		System.out.println("peek(): " + heap.peek());
		System.out.println("======= poll()");
		while (!heap.isEmpty()) {
			// 返回最小值，抛出队列
			System.out.println(heap.poll());
		}
		System.out.println("======= id sort");
		PriorityQueue<Student> stuHeap = new PriorityQueue<>(new IdComparator());
		stuHeap.add(s1);
		stuHeap.add(s2);
		stuHeap.add(s3);
		stuHeap.add(s4);
		stuHeap.add(s5);
		while (!stuHeap.isEmpty()) {
			Student s = stuHeap.poll();
			System.out.println(s.name + ", " + s.id + ", " + s.age);
		}
		
		// 类似 TreeSet<>(new IdComparator()) 等系统自带的结构，都可以使用自定义比较器
	}
}
