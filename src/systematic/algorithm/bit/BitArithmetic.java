package systematic.algorithm.bit;

/**
 * 位运算
 * 
 * @author lisy
 */
public class BitArithmetic {

	public static void print(int num) {
		for (int i = 31; i >= 0; i--) {
			System.out.print((num & (1 << i)) == 0 ? "0" : "1");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		/**
		 * 异或特点:(无进位相加)
		 * 1. 0 ^ N = N
		 * 2. N ^ N = 0
		 * 3. 满足交换律 a ^ b = b ^ a
		 * 4. 满足结合律 (a ^ b) ^ c = a ^ (b ^ c)
		 */
		System.out.println("------异或-----");
		int a = 7;
		int b = 13;
		print(a);
		print(b);
		print(a ^ b);
		
	}
}
